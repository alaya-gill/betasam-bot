import json
import pandas as pd
import datetime
from datetime import timedelta
import os,glob
import requests
def readFile():
    df = pd.read_excel('./apis.xlsx')
    return df

def months_between(start_date, end_date):
    if start_date > end_date:
        raise ValueError(f"Start date {start_date} is not before end date {end_date}")

    year = start_date.year
    month = start_date.month

    while (year, month) <= (end_date.year, end_date.month):
        yield datetime.date(year, month, 1)

        # Move to the next month.  If we're at the end of the year, wrap around
        # to the start of the next.
        #
        # Example: Nov 2017
        #       -> Dec 2017 (month += 1)
        #       -> Jan 2018 (end of year, month = 1, year += 1)
        #
        if month == 12:
            month = 1
            year += 1
        else:
            month += 1

def getDates():
    start_of_2020 = datetime.date(2018, 12, 1)
    today = datetime.date(2019,12,1)
    dates=[]
    for month in months_between(start_of_2020, today):
        dates.append(str(month.strftime("%m/%d/%Y")))
    
    return dates
def validateCoolDown(df):
    now=datetime.datetime.now().strftime('%m/%d/%Y, %H:%M:%S')
    NUMBER_OF_SECONDS = 86400 # seconds in 24 hours
    for idx,(req,elapse) in enumerate(zip(df['requests'],df['elapse'])):
        if req==10 and str(elapse)!="0":
            elapsed=datetime.datetime.strptime(elapse, '%d/%m/%Y, %H:%M:%S')
            rn=datetime.datetime.strptime(now, '%m/%d/%Y, %H:%M:%S')
            if (rn-elapsed).total_seconds() >= NUMBER_OF_SECONDS:
                df['requests'][idx]=0
                df['elapse'][idx]=0
                #req=df['requests'][idx]
    return df
            
def getApiIndexes(df):
    apis=[]
    df=validateCoolDown(df)
    for idx,(req,elapse) in enumerate(zip(df['requests'],df['elapse'])):
        if req<10 and str(elapse)=='0':
            req=df['requests'][idx]
            apis.append(idx)
    return apis

def backlog():
    return os.stat("./backlog.txt").st_size == 0
def hitFirstRequest(api,to_date,from_date):
    print(api,1)
    offset=0
    limit=1000
    print(api)
    records=0
    url=f"""https://api.sam.gov/prod/opportunities/v1/search?api_key={api}&postedFrom={from_date}&postedTo={to_date}&limit={str(limit)}&offset={str(offset)}"""
    r = requests.get(url)
    body = r.json()

    records=body['totalRecords']
    file_count=0
    date=""
    if from_date.split('/')[0]=='12':
        date=to_date.split('/')[2]
    else:
        date=to_date.split('/')[2]

    # try:
    #     list_of_files = glob.glob('./'+str(date)+'/*.json') # * means all if need specific format then *.csv
    #     latest_file = max(list_of_files, key=os.path.getctime)
    #     file_count=(int(latest_file.split('\\')[1][0]))
    # except:
    #     file_count=0
    file_count = len(glob.glob1('./'+str(date)+'/',"*.json"))
    
    with open("./"+str(date)+'/'+str(file_count+1)+'.json', "w+") as f:
        json.dump(body, f)
    if records<=1000:
        return records,0,1000

    return records,1000,1000
def truncateBacklog():
    fo = open("backlog.txt", "w")
    fo.truncate()
    fo.close()
def writeInBacklog(records,to_date,from_date,offset,limit):
    f= open("backlog.txt","w+")
    f.write(str(records)+'\n'+from_date+'\n'+to_date+'\n'+str(offset)+'\n'+str(limit))
    f.close()
def hitRequest(api,to_date,from_date,idx,offset,limit):
    ##########hit on url###############
    print(api,2)
    url=f"""https://api.sam.gov/prod/opportunities/v1/search?api_key={api}&postedFrom={from_date}&postedTo={to_date}&limit={str(limit)}&offset={str(offset)}"""
    r = requests.get(url)
    body = r.json()
    file_count=0
    date=""
    if from_date.split('/')[0]=='12':
        date=to_date.split('/')[2]
    else:
        date=to_date.split('/')[2]

    # try:
    #     list_of_files = glob.glob('./'+str(date)+'/*.json') # * means all if need specific format then *.csv
    #     latest_file = max(list_of_files, key=os.path.getctime)
    #     file_count=(int(latest_file.split('\\')[1][0]))
    # except:
    #     file_count=0
    file_count = len(glob.glob1('./'+str(date)+'/',"*.json"))
    
    with open("./"+str(date)+'/'+str(file_count+1)+'.json', "w+") as f:
        json.dump(body, f)
if __name__ == "__main__":
    dates=getDates()
    print(dates)
    df=readFile()
    apiKeys=[api for api in df['api key']]
    validApis=getApiIndexes(df)
    print(validApis)
    to_date=""
    from_date=""
    checkRecords=True
    #count=len(dates)-1
    count=0
    offset=0
    limit=1000
    noOfApis=len(validApis)
    apiCount=0
    dateChanged=False
    records=0
    print(apiKeys)
    if len(validApis)>0:
        print("in")
        while apiCount <= noOfApis: #Step 1 Done with ValidApis
            if not backlog():       #Step 2 
                print(True)
                ###########read backlog###########
                fo = open("backlog.txt", "r")
                lines=fo.readlines()
                fo.close()
                records,from_date,to_date,offset,limit=int(lines[0].replace('\n','')),lines[1].replace('\n',''),lines[2].replace('\n',''),int(lines[3].replace('\n','')),1000
                dateChanged=False
            else:
                if checkRecords:
                    try:
                        print("from: ",dates[count],"to: ",dates[count+1])
                    except:
                        print(df)
                        df.to_excel("output.xlsx") 
                        break
                    from_date=dates[count]
                    to_date=dates[count+1]
                    dateChanged=True
                    offset=max(0,offset)
                    limit=1000
                    count=count+1
        #####################url#############
            #url=""
            if dateChanged:
                try:
                    records,offset,limit=hitFirstRequest(apiKeys[validApis[apiCount]],to_date,from_date)
                    checkRecords=True
                except:
                    df['requests'][validApis[apiCount]]=10
                    df['elapse'][validApis[apiCount]]=datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S")
                    offset,limit=0,1000
                    apiCount+=1
                    dateChanged=True
                    checkRecords=False
                    continue
                df['requests'][validApis[apiCount]]=int(df['requests'][validApis[apiCount]])+1
                dateChanged=False
                if records<=1000:
                    dateChanged=True
                    #count=count+1
                    checkRecords=True
                    offset=0
                    limit=1000
                    continue
                records=records-limit #Changed 
            req=df['requests'][validApis[apiCount]]
            
            if req==10:
                df['elapse'][validApis[apiCount]]=datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S")
                apiCount+=1
                checkRecords=False
                dateChanged=False
                continue
            for x in range(10-req):
                if records<=0:
                        checkRecords=True
                        dateChanged=True
                        #count=count+1
                        offset=0
                        limit=1000
                        truncateBacklog()
                        break
                hitRequest(apiKeys[validApis[apiCount]],to_date,from_date,validApis[apiCount],offset,limit)
                records=records-limit
                offset+=1000
                limit=1000
                df['requests'][validApis[apiCount]]=df['requests'][validApis[apiCount]]+1
                print(records,offset,df['requests'][validApis[apiCount]])
                if df['requests'][validApis[apiCount]]==10:
                        df['elapse'][validApis[apiCount]]=datetime.datetime.now().strftime("%d/%m/%Y, %H:%M:%S")
                        apiCount+=1
                        if records>0: #Changed
                            if apiCount==len(validApis):
                                writeInBacklog(records,to_date,from_date,offset,limit)
                            checkRecords=False
                            dateChanged=False
                            break
                        elif records<=0:
                            checkRecords=True
                            dateChanged=True
                            #count=count+1
                            offset=0
                            limit=1000
                            truncateBacklog()
                            break
                if records<=0:
                        checkRecords=True
                        dateChanged=True
                        #count=count+1
                        offset=0
                        limit=1000
                        truncateBacklog()
                        break











